<?php

namespace Calculator;

use Calculator\Exception\CalculatorException;
use Calculator\Interfaces\IOperator;
use Calculator\Interfaces\IValidator;

class Calculator implements IValidator
{
    /**
     * @var string
     */
    private $expression = "";

    /**
     * @var array
     */
    private $reversePolishExpression = [];

    /**
     * @var IOperator[]
     */
    private $customOperators = [];

    /**
     * @var array
     */
    private $operators = ["+", "-", "*", "/"];

    /**
     * @var array
     */
    private $openingBrackets = ['(', '['];

    /**
     * @var array
     */
    private $closingBrackets = [')', ']'];

    /**
     * @param string $expression
     * @return bool
     */
    public function isValidMathOperatorPositions(string $expression): bool
    {
        //Первый и последний символ не могут быть операторами
        if ($this->isSymbolOperator($expression[0]) || $this->isSymbolOperator($expression[strlen($expression) - 1])) {
            return false;
        }

        for ($i = 0; $i < strlen($expression); $i++) {
            $current = $expression[$i];
            $next = $expression[$i + 1] ?? null;
            $previous = $i > 0 ? $expression[$i - 1] : null;

            //Текуший символ - знак, и следующий знак (ошибка)
            if ($this->isSymbolOperator($current) && $next && $this->isSymbolOperator($next)) {
                return false;
            }

            //Текуший символ - знак, а следующий закрывающая скобка (ошибка)
            if ($this->isSymbolOperator($current) && $next && $this->isSymbolBracket($next, self::BRACKET_CLOSE)) {
                return false;
            }

            if (
                $this->isSymbolBracket($current, self::BRACKET_CLOSE) &&
                $next &&
                !$this->isSymbolOperator($next) &&
                !$this->isSymbolBracket($next, self::BRACKET_ANY)
            ) {
                return false;
            }

            if (
                $this->isSymbolBracket($current, self::BRACKET_OPEN) &&
                $previous &&
                !$this->isSymbolOperator($previous) &&
                !$this->isSymbolBracket($previous, self::BRACKET_ANY)
            ) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param string $expression
     * @return bool
     */
    public function isValidExpression(string $expression): bool
    {
        $customNames = array_map(function (IOperator $operator) {
            return '[' . $operator->getName() . ']';
        }, $this->customOperators);

        if ($customNames) {
            $customNames = '|' . implode('|', $customNames);
        }

        $regex = "/(?!\+|\-|\*|\/|\d+|$|\s|\(|\)|\[|\]|\,{$customNames})/uU";

        preg_match_all($regex, $expression, $matches);
        $matches = array_filter($matches);

        return $matches ? false : true;
    }

    /**
     * @param string $expression
     * @return bool
     */
    public function isValidBrackets(string $expression): bool
    {
        $stack = [];

        for ($i = 0; $i < strlen($expression); $i++) {
            if (in_array($expression[$i], $this->openingBrackets)) {
                $stack[] = $this->openingBrackets[array_search($expression[$i], $this->openingBrackets)];
            }

            if (in_array($expression[$i], $this->closingBrackets)) {
                $relativeOpenBracket = $this->openingBrackets[array_search($expression[$i], $this->closingBrackets)];
                $lastBracket = end($stack);

                if ($stack && $relativeOpenBracket === $lastBracket) {
                    array_pop($stack);
                } else {
                    return false;
                }
            }
        }

        if (!$stack) {
            return true;
        }

        return false;
    }

    /**
     * @param string $expression
     * @return float
     * @throws CalculatorException
     */
    public function calculate(string $expression)
    {
        $this->expression = $expression;
        $this->trimExpression();

        if (!$this->isValidExpression($this->expression)) {
            throw new CalculatorException(sprintf('Invalid symbols in expression "%s"', $expression));
        }

        if (!$this->isValidBrackets($this->expression)) {
            throw new CalculatorException(sprintf('Invalid bracket composition in expression "%s"', $expression));
        }

        $this->processCustomOperators();

        if (!$this->isValidMathOperatorPositions($this->expression)) {
            throw new CalculatorException(sprintf('Invalid math in expressions "%s"', $expression));
        }

        $this->reversePolishExpression = $this->createReversePolishNotation();
        $stack = [];

        while ($element = array_shift($this->reversePolishExpression)) {
            if (is_numeric($element)) {
                $stack[] = $element;
            }

            if ($this->isSymbolOperator($element)) {
                $firstArgument = $stack[count($stack) - 2];
                $secondArgument = $stack[count($stack) - 1];

                switch ($element) {
                    case '+':
                        $result = $firstArgument + $secondArgument;
                        break;
                    case '-':
                        $result = $firstArgument - $secondArgument;
                        break;
                    case '*':
                        $result = $firstArgument * $secondArgument;
                        break;
                    case '/':
                        $result = $firstArgument / $secondArgument;
                        break;
                    default:
                        $result = 0;
                }

                array_pop($stack);
                array_pop($stack);
                $stack[] = $result;
            }
        }

        $result = reset($stack);
        return $result;
    }

    /**
     * @param IOperator $operator
     */
    public function addOperator(IOperator $operator)
    {
        $this->customOperators[$operator->getName()] = $operator;
    }

    /**
     * Считаем результаты функций поскольку их приортет всегда наивсший
     * то подсчет делаем до того как дошли до польской нотации
     */
    private function processCustomOperators()
    {
        foreach ($this->customOperators as $operator) {
            $regex = "/{$operator->getName()}\((.*)\)/Uu";
            preg_match_all($regex, $this->expression, $matches);

            foreach ($matches[0] as $k => $operatorBody) {
                $arguments = explode(',', $matches[1][$k]);

                if ($operator->getArgumentCount() !== count($arguments)) {
                    throw new CalculatorException(sprintf(
                        "{$operator->getName()} accepts {$operator->getArgumentCount()} arguments, %s given",
                        count($arguments)
                    ));
                }

                $operatorResult = $operator->calc($arguments);
                $this->expression = str_replace($operatorBody, $operatorResult, $this->expression);
            }
        }
    }

    /**
     * @param string $symbol
     * @param int $bracketType
     * @return bool
     */
    private function isSymbolBracket(string $symbol, int $bracketType = -1)
    {
        switch ($bracketType) {
            case IValidator::BRACKET_ANY:
                return in_array($symbol, array_merge($this->openingBrackets, $this->closingBrackets));
            case IValidator::BRACKET_OPEN:
                return in_array($symbol, $this->openingBrackets);
            case IValidator::BRACKET_CLOSE:
                return in_array($symbol, $this->closingBrackets);
            default:
                return false;
        }
    }

    /**
     * @param string $symbol
     * @return bool
     */
    private function isSymbolOperator(string $symbol)
    {
        return in_array($symbol, $this->operators);
    }

    /**
     * Убираем лишние пробельные символы
     */
    private function trimExpression()
    {
        $this->expression = preg_replace('/\s+/', '', $this->expression);
    }

    /**
     * Преобразование в обратную польскую нотацию
     */
    private function createReversePolishNotation()
    {
        $stack = [];
        $result = [];

        $priority = [
            '*' => 1,
            '/' => 1,
            '+' => 0,
            '-' => 0
        ];

        preg_match_all('/(\+|\-|\*|\/|\d+|\(|\)|\[|\]|.)/u', $this->expression, $matches);

        $array = $matches[0];

        //Преобразование дробных
        foreach ($array as $k => $v) {
            if ($v === ".") {
                $number = (float)($array[$k - 1] . '.' . $array[$k + 1]);

                unset($array[$k - 1]);
                unset($array[$k]);
                unset($array[$k + 1]);

                $array[$k] = $number;
            }
        }

        ksort($array);
        $array = array_values($array);

        foreach ($array as $item) {
            if (is_numeric($item)) {
                $result[] = $item;
            }

            if ($this->isSymbolOperator($item)) {
                $last = end($stack);

                while ($last && $this->isSymbolOperator($last) && $priority[$item] <= $priority[$last]) {
                    $result[] = array_pop($stack);
                    $last = $stack ? end($stack) : null;
                }

                $stack[] = $item;
            }

            if ($this->isSymbolBracket($item, IValidator::BRACKET_OPEN)) {
                $stack[] = $item;
            }

            if ($this->isSymbolBracket($item, IValidator::BRACKET_CLOSE)) {
                $last = array_pop($stack);

                while ($last && !$this->isSymbolBracket($last, IValidator::BRACKET_OPEN) && $last) {
                    $result[] = $last;
                    $last = $stack ? array_pop($stack) : null;
                }

            }
        }

        if ($stack) {
            $result = array_merge($result, array_reverse($stack));
        }

        return $result;
    }
}