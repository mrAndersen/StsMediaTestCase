<?php

namespace Calculator\Operator;

use Calculator\Interfaces\IOperator;

class Sqrt implements IOperator
{

    /**
     * @return int
     */
    public function getArgumentCount(): int
    {
        return 1;
    }

    /**
     * @param array $arguments
     * @return float
     */
    public function calc(array $arguments): float
    {
        return sqrt($arguments[0]);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return 'sqrt';
    }
}