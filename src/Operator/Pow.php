<?php

namespace Calculator\Operator;

use Calculator\Interfaces\IOperator;

class Pow implements IOperator
{
    /**
     * @return int
     */
    public function getArgumentCount(): int
    {
        return 2;
    }

    /**
     * @param $arguments
     * @return float
     */
    public function calc(array $arguments): float
    {
        return pow($arguments[0], $arguments[1]);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return 'pow';
    }
}