<?php

namespace Calculator\Interfaces;


interface IOperator
{
    /**
     * @return int
     */
    public function getArgumentCount(): int;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @param array $arguments
     * @return float
     */
    public function calc(array $arguments): float;

}