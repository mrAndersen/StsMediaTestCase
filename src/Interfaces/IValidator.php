<?php

namespace Calculator\Interfaces;


use Calculator\Exception\CalculatorException;

interface IValidator
{
    const BRACKET_OPEN = 1;
    const BRACKET_CLOSE = 0;
    const BRACKET_ANY = -1;

    /**
     * Проверяем наличие валидных символов (только арифметические и цифры (не флоаты))
     * @param string $expression
     * @return bool
     */
    public function isValidExpression(string $expression): bool;

    /**
     * Проверям валидность скобок
     * @param string $expression
     * @return bool
     */
    public function isValidBrackets(string $expression): bool;

    /**
     * Проверяем валидность положения символов т.е. после "+" скобка или число, но не другой знак
     * @param string $expression
     * @return bool
     */
    public function isValidMathOperatorPositions(string $expression): bool;
}