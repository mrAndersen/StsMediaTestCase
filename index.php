<?php

use Calculator\Calculator;
use Calculator\Exception\CalculatorException;
use Calculator\Operator\Pow;
use Calculator\Operator\Sqrt;

require_once 'vendor/autoload.php';

try {
    $calculator = new Calculator();
    $calculator->addOperator(new Pow());
    $calculator->addOperator(new Sqrt());

    $results[] = $calculator->calculate("(3-4) * (6 / 3)");

    print_r($results);
} catch (CalculatorException $exception) {
    echo "Exception during calculation: " . $exception->getMessage();
} catch (\Exception $exceptionInternal) {
    echo $exceptionInternal->getMessage();
}


