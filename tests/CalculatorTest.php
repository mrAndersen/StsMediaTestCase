<?php
declare(strict_types=1);

namespace Tests;

use Calculator\Calculator;
use Calculator\Exception\CalculatorException;
use Calculator\Operator\Pow;
use Calculator\Operator\Sqrt;
use PHPUnit\Framework\TestCase;

class CalculatorTest extends TestCase
{
    /**
     * @expectedException  Calculator\Exception\CalculatorException
     */
    public function testError()
    {
        $calculator = new Calculator();
        $calculator->addOperator(new Pow());
        $results[] = $calculator->calculate('pow(2,3) + 3 / pow(5,3) * 2 + sqrt(2) * sqrt(712)');
    }

    /**
     * @expectedException Calculator\Exception\CalculatorException
     */
    public function testError2()
    {
        $this->expectException(CalculatorException::class);

        $calculator = new Calculator();
        $calculator->addOperator(new Pow());
        $results[] = $calculator->calculate('2+3)');
    }

    /**
     * @expectedException Calculator\Exception\CalculatorException
     */
    public function testError3()
    {
        $calculator = new Calculator();
        $calculator->addOperator(new Pow());
        $results[] = $calculator->calculate('3 * 4 + (4 +');
    }

    public function test()
    {
        $calculator = new Calculator();
        $calculator->addOperator(new Pow());
        $calculator->addOperator(new Sqrt());

        $results[] = $calculator->calculate("[(3-4) * (6 / 3)/ [4 + 6] * [18 - 9]] * 22 / 1 / 2 / 3 / 4 * (2 - 8) + 1 / 982");
        $results[] = $calculator->calculate("[(3-4) * (6 / 3)] + 2");
        $results[] = $calculator->calculate("3 - 2 * 5 + 6 / 2 + [[2 - 3] / (90 - 801)]");
        $results[] = $calculator->calculate("3 / 4 * (2 - 11) * 12 / 4 / 12 + ((22 + 2)/3)/4");
        $results[] = $calculator->calculate('pow(2,3) + 3 / pow(5,3) * 2 + sqrt(2) * sqrt(712)');

        $this->assertEquals(9.901, $results[0], '', 0.001);
        $this->assertEquals(0, $results[1], '', 0.001);
        $this->assertEquals(-3.998, $results[2], '', 0.001);
        $this->assertEquals(0.3125, $results[3], '', 0.001);
        $this->assertEquals(45.783, $results[4], '', 0.001);
    }
}