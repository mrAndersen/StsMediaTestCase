#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

apt-get update -y
apt-get install -y git zlib1g-dev

curl --location --output /usr/local/bin/phpunit https://phar.phpunit.de/phpunit.phar
chmod +x /usr/local/bin/phpunit

curl --location --output /usr/local/bin/composer https://getcomposer.org/download/1.6.5/composer.phar
chmod +x /usr/local/bin/composer

docker-php-ext-install zip